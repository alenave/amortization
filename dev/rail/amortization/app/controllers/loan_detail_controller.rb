require 'amortization.rb'
class LoanDetailController < ApplicationController
  def index
    @loanDetails = LoanDetail.all
  end

  def show
  	@loanDetail = LoanDetail.find(params[:id])
  	@payment_details = Amortization.calculate(@loanDetail)
  end

  def new
  	@loanDetail = LoanDetail.new
  end

  def create
  	@loanDetail = LoanDetail.new(loan_detail_params)
  	if @loanDetail.save
  	  redirect_to @loanDetail
    else
      render 'new'
    end
  end

  def edit
    @loanDetail = LoanDetail.find(params[:id])
  end

  def update
    @loanDetail = LoanDetail.find(params[:id])
   
    if @loanDetail.update(loan_detail_params)
      redirect_to @loanDetail
    else
      render 'edit'
    end
  end

  def destroy
    @loanDetail = LoanDetail.find(params[:id])
    @loanDetail.destroy
    redirect_to loan_detail_index_path
  end

  private 
  def loan_detail_params
  	params.require(:loanDetail).permit(:loan_amount, :tenure, :rate_of_interest, :one_time_payment, :set_date_one_time_payment, :monthly_payment, :yearly_payment, :set_date_yearly_payment)
  end



end
